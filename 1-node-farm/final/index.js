// const fs = require('fs');
// const http = require('http');
// const url = require('url');
// const slugify = require('slugify');
// const replaceTemplate = require('./modules/replaceTemplate');

/////////////////////////////////
// FILES

// Blocking, synchronous way
// const textIn = fs.readFileSync('./txt/input.txt', 'utf-8');
// console.log(textIn);
// const textOut = `This is what we know about the avocado: ${textIn}.\nCreated on ${Date.now()}`;
// fs.writeFileSync('./txt/output.txt', textOut);
// console.log('File written!');

// Non-blocking, asynchronous way
// fs.readFile('./txt/start.txt', 'utf-8', (err, data1) => {
//   if (err) return console.log('ERROR! 💥');

//   fs.readFile(`./txt/${data1}.txt`, 'utf-8', (err, data2) => {
//     console.log(data2);
//     fs.readFile('./txt/append.txt', 'utf-8', (err, data3) => {
//       console.log(data3);

//       fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', err => {
//         console.log('Your file has been written 😁');
//       })
//     });
//   });
// });
// console.log('Will read file!');

/////////////////////////////////
// SERVER
// const tempOverview = fs.readFileSync(
//   `${__dirname}/templates/template-overview.html`,
//   'utf-8'
// );
// const tempCard = fs.readFileSync(
//   `${__dirname}/templates/template-card.html`,
//   'utf-8'
// );
// const tempProduct = fs.readFileSync(
//   `${__dirname}/templates/template-product.html`,
//   'utf-8'
// );

// const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8');
// const dataObj = JSON.parse(data);

// const slugs = dataObj.map(el => slugify(el.productName, { lower: true }));
// console.log(slugs);

// const server = http.createServer((req, res) => {
//   const { query, pathname } = url.parse(req.url, true);

//   // Overview page
//   if (pathname === '/' || pathname === '/overview') {
//     res.writeHead(200, {
//       'Content-type': 'text/html'
//     });

//     const cardsHtml = dataObj.map(el => replaceTemplate(tempCard, el)).join('');
//     const output = tempOverview.replace('{%PRODUCT_CARDS%}', cardsHtml);
//     res.end(output);

//     // Product page
//   } else if (pathname === '/product') {
//     res.writeHead(200, {
//       'Content-type': 'text/html'
//     });
//     const product = dataObj[query.id];
//     const output = replaceTemplate(tempProduct, product);
//     res.end(output);

//     // API
//   } else if (pathname === '/api') {
//     res.writeHead(200, {
//       'Content-type': 'application/json'
//     });
//     res.end(data);

//     // Not found
//   } else {
//     res.writeHead(404, {
//       'Content-type': 'text/html',
//       'my-own-header': 'hello-world'
//     });
//     res.end('<h1>Page not found!</h1>');
//   }
// });

// server.listen(8000, '127.0.0.1', () => {
//   console.log('Listening to requests on port 8000');
// });

const fs = require('fs');
const http = require('http');
const url = require('url');
// Synchronus way
// const inpuTxt = fs.readFileSync('./txt/input.txt','utf-8');
// console.log(inpuTxt);

// const txtWrite = `Hi My Name is Srivatav Susheel Jehsval ${inpuTxt}.\n Created on ${Date.now()}`;
// fs.writeFileSync('./txt/outWithNew.txt',txtWrite);

// console.log("blocking content");

// ASynchronus way
// nodejs read files background

// fs.readFile('./txt/start.txt','utf-8',(error,data1) => {
//   console.log(data1 +' '+ "data1")
//    fs.readFile(`./txt/${data1}.txt`,'utf-8',(err,data2) => {
//      console.log(data2 +' '+ "data2");
//      fs.readFile('./txt/append.txt','utf-8',(err,data3) => {
//        console.log(data3 +' '+ "data3");
//        fs.writeFile('./txt/final1.txt',`${data2}\n${data3}`,'utf-8',err => {
//          console.log("final text display")
//        })
//      })
//    })
// })

// creating server

const server = http.createServer((req,res) => {
  console.log(req.url);
  const pathname = req.url;
  if(pathname === '/' || pathname === '/ss'){
    res.end('Moving to SS');
  }else if(pathname === '/susheel'){
    res.end('Moving to Susheel Page');
  }else{
    res.writeHead(404,{
      'Content-type': 'text/html',
      'my-own-header': 'hello-world'
    });
    res.end('Hello To Node Server');
  }
 
})

server.listen(3000,'127.0.0.1',() => {
  console.log('new server started 8080');
})

